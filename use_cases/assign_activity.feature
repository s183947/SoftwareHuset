Feature: Add employees to the activity
        Description: Employees are assigned an activity by another Employee
        Actors: Employee
        
Scenario: Add an employee successfully
         Given there is an activity "HEJSA"
         And the employee with ID "gjj" exist
         And the employee "gjj" is not already assigned to the activity
         When I assign the activity
         Then the activity is assigned to the employee
         
         
Scenario: Add an employee when the employee is not logged in
         Given there is no activity "Doesnt exist"
         And the employee with ID "gjj" exist
         And the employee "gjj" is not already assigned to the activity
         When I assign the activity
         Then i get the error message "Employee or activity doesnt exist or activity is already assigned"
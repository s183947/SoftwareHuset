Feature: Add employees to the project
        Description: Employees are added to the project
        Actors: Employee
        
        
Scenario: Add an employee successfully
         Given the employee "gjj" logs in
         When I add the employee "Random employee"
        Then the employee "Random employee" exist
    
         
Scenario: Add an employee when the employee is not logged in
        Given that the employee is not logged in
        When I add the employee "Random employee"
        Then I get an error message "Employee must be logged in"
             

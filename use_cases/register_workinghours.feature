Feature: Register working hours
        Description: Employee registers working hours on activity
        Actors: Employee
        
           
         
Scenario: Register working hours 
        Given the employee "gjj" logs in
        And I have an activity "Design"
        When I register "3" working hours
        Then the working hours are registered with ID "gjj" and activity "Design"
             

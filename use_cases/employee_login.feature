Feature: Employee login
	Description: The employee logs into the system and also logs out
	Actor: Employee	
	
Scenario: Employee can login
	Given that the employee is not logged in
	And the employee with ID "gjj" exist
	Then the employee "gjj" logs in
	And the employee is logged in
	
	Scenario: Employee has the wrong username
	Given that the employee is not logged in
	And the employee "Something" doesnt exist
	When the employee "Something" logs in
	Then the employee is not logged in





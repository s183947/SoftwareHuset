package dtu.softwarehouse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

class ReadWriteFiles {
    static void createDirectoryAtPath(String path, String directoryName) {
        Path dirPath = getPath(path, directoryName);
        if (Files.notExists(dirPath)) {
            try {
                Files.createDirectory(dirPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static void createTxtFileAtPath(String path, String fileName) {
        Path filePath = Paths.get(path + "/" + fileName + ".txt");
        if (Files.notExists(filePath)) {
            try {
                Files.createFile(filePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static void writeLineToFile(String path, String fileName, String textLine) {
        Path filePath = getPath(path, fileName + ".txt");
        List<String> newLines = readFromFile(path, fileName);
        newLines.add(textLine);
        writeTheText(filePath, newLines);
    }

    static void writeLineToFile(String path, String fileName, String textLine, int lineNr) {
        Path filePath = getPath(path, fileName + ".txt");
        List<String> newLines = readFromFile(path, fileName);
        newLines.add(lineNr, textLine);
        writeTheText(filePath, newLines);
    }

    static void editTextAtLine(String path, String fileName, String newTextLine, int lineNr) {
        Path filePath = getPath(path, fileName + ".txt");
        List<String> newLines = readFromFile(path, fileName);
        newLines.set(lineNr, newTextLine);
        writeTheText(filePath, newLines);
    }

    private static Path getPath(String path, String fileName) {
        Path filePath;
        if (path.isEmpty()) {
            filePath = Paths.get(fileName);
        } else {
            filePath = Paths.get(path + "/" + fileName);
        }
        return filePath;
    }

    private static void writeTheText(Path filePath, List<String> newLines) {
        try {
            Files.write(filePath, newLines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static List<String> readFromFile(String path, String fileName) {
        Path filePath = Paths.get(path + "/" + fileName + ".txt");
        List<String> lines = null;
        try {
            lines = Files.readAllLines(filePath, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}

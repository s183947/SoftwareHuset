package dtu.softwarehouse;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

class mainProgramScreen extends JFrame {
    private JButton viewActivitiesButton, workedHoursButton, addActivityButton, newEmployeeButton, activityWorkedHoursButton, newActivityButton, employeeWorkingHoursButton;
    private JTextField addActivityField1;
    private JTextField addActivityField2;
    private JTextField newEmployeeField;
    private JTextField workedHoursField1;
    private JTextField workedHoursField2;
    private JTextField activityWorkedHoursField;
    private JTextField newActivityField;
    private JTextField employeeWorkingHoursField;
    private JTextArea viewActivitiesArea;

    mainProgramScreen() {
        JTabbedPane tabbedPane = new JTabbedPane();
        JPanel viewActivitiesPanel = new JPanel();
        viewActivitiesArea = new JTextArea();
        viewActivitiesArea.setEnabled(false);
        viewActivitiesButton = new JButton("Reset");
        viewActivitiesPanel.setLayout(createSubmissionForm(viewActivitiesPanel, viewActivitiesArea, viewActivitiesButton));
        JPanel workedHoursPanel = new JPanel();
        workedHoursField1 = new JTextField();
        workedHoursField1.setMaximumSize(new Dimension(1000, 20));
        workedHoursField2 = new JTextField();
        workedHoursField2.setMaximumSize(new Dimension(1000, 20));
        workedHoursButton = new JButton("Go");
        workedHoursPanel.setLayout(createSubmissionForm(workedHoursPanel, new JLabel("Activity name"), new JLabel("Used time:"), workedHoursField1, workedHoursField2, workedHoursButton));
        JPanel addActivityPanel = new JPanel();
        addActivityField1 = new JTextField();
        addActivityField2 = new JTextField();
        addActivityButton = new JButton("Go");
        addActivityPanel.setLayout(createSubmissionForm(addActivityPanel, new JLabel("Activity name:"), new JLabel("Employee ID:"), addActivityField1, addActivityField2, addActivityButton));
        JPanel newEmployeePanel = new JPanel();
        newEmployeeField = new JTextField();
        newEmployeeButton = new JButton("Go");
        newEmployeePanel.setLayout(createSubmissionForm(newEmployeePanel, new JLabel("Full name:"), newEmployeeField, newEmployeeButton));
        JPanel newActivityPanel = new JPanel();
        newActivityField = new JTextField();
        newActivityButton = new JButton("Go");
        newActivityPanel.setLayout(createSubmissionForm(newActivityPanel, new JLabel("Activity name:"), newActivityField, newActivityButton));
        JPanel activityWorkingHoursPanel = new JPanel();
        activityWorkedHoursField = new JTextField();
        activityWorkedHoursButton = new JButton("Go");
        activityWorkingHoursPanel.setLayout(createSubmissionForm(activityWorkingHoursPanel, new JLabel("Activity name:"), activityWorkedHoursField, activityWorkedHoursButton));
        JPanel employeeWorkingHoursPanel = new JPanel();
        employeeWorkingHoursField = new JTextField();
        employeeWorkingHoursButton = new JButton("Go");
        employeeWorkingHoursPanel.setLayout(createSubmissionForm(employeeWorkingHoursPanel, new JLabel("Employee name:"), employeeWorkingHoursField, employeeWorkingHoursButton));

        tabbedPane.addTab("View activities", viewActivitiesPanel);
        tabbedPane.addTab("Register hours", workedHoursPanel);
        tabbedPane.addTab("Give Activity", addActivityPanel);
        tabbedPane.addTab("New employee", newEmployeePanel);
        tabbedPane.addTab("New activity", newActivityPanel);
        tabbedPane.addTab("Worked on activity", activityWorkingHoursPanel);
        tabbedPane.addTab("Working hours", employeeWorkingHoursPanel);
        getContentPane().add(tabbedPane, BorderLayout.CENTER);
    }

    private GroupLayout createSubmissionForm(JPanel p1, JTextArea a1, JButton b1) {
        GroupLayout layout = new GroupLayout(p1);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        layout.setHorizontalGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(a1).addComponent(b1)));
        layout.setVerticalGroup(layout.createSequentialGroup().addComponent(a1).addComponent(b1));
        return layout;
    }

    private GroupLayout createSubmissionForm(JPanel p1, JLabel l1, JLabel l2, JTextField t1, JTextField t2, JButton b1) {
        GroupLayout layout = new GroupLayout(p1);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        layout.setHorizontalGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(l1).addComponent(l2)).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(t1).addComponent(t2)).addComponent(b1));
        layout.setVerticalGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(l1).addComponent(t1)).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(l2).addComponent(t2).addComponent(b1)));
        return layout;
    }

    private GroupLayout createSubmissionForm(JPanel p1, JLabel l1, JTextField t1, JButton b1) {
        GroupLayout layout = new GroupLayout(p1);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        layout.setHorizontalGroup(layout.createSequentialGroup().addComponent(l1).addComponent(t1).addComponent(b1));
        layout.setVerticalGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(l1).addComponent(t1).addComponent(b1)));
        return layout;
    }

    void addViewActivitiesButton(ActionListener e) {
        viewActivitiesButton.addActionListener(e);
    }

    void addWorkedHoursButton(ActionListener e) {
        workedHoursButton.addActionListener(e);
    }

    void addAddActivityButton(ActionListener e) {
        addActivityButton.addActionListener(e);
    }

    void addNewEmployeeButton(ActionListener e) {
        newEmployeeButton.addActionListener(e);
    }

    void addActivityWorkedHoursButton(ActionListener e) {
        activityWorkedHoursButton.addActionListener(e);
    }

    void addNewActivityButton(ActionListener e) {
        newActivityButton.addActionListener(e);
    }

    void addEmployeeWorkedHoursButton(ActionListener e) {
        employeeWorkingHoursButton.addActionListener(e);
    }


    void setupAssignedScreen(String id) {
        viewActivitiesArea.setText("");
        List<String> assigned = ReadWriteFiles.readFromFile("employees", id + "_assigned");
        for (String line : assigned) {
            viewActivitiesArea.append(line + "\r\n");
        }
    }

    String getWorkedHoursField1() {
        return workedHoursField1.getText();
    }

    String getWorkedHoursField2() {
        return workedHoursField2.getText();
    }

    String getAddActivityField1() {
        return addActivityField1.getText();
    }

    String getAddActivityField2() {
        return addActivityField2.getText();
    }

    String getNewEmployeeNewField() {
        return newEmployeeField.getText();
    }

    String getActivityWorkedHours() {
        return activityWorkedHoursField.getText();
    }

    String getNewActivity() {
        return newActivityField.getText();
    }

    String getEmployeeWorkingHours() {
        return employeeWorkingHoursField.getText();
    }


    void clearAllTextField() {
        workedHoursField1.setText("");
        workedHoursField2.setText("");
        addActivityField1.setText("");
        addActivityField2.setText("");
        addActivityField2.setText("");
        newEmployeeField.setText("");
    }
}


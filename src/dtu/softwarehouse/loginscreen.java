package dtu.softwarehouse;

import javax.swing.*;
import java.awt.event.ActionListener;

class loginScreen extends JFrame {

    private JLabel l2;
    private JButton b1;
    private JTextField t1;

    loginScreen() {
        JPanel p1 = new JPanel();
        t1 = new JTextField();
        b1 = new JButton("    Go    ");
        JLabel l1 = new JLabel("Enter ID:");
        l2 = new JLabel("");
        GroupLayout layout = new GroupLayout(p1);
        p1.setLayout(layout);

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addComponent(l1)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(t1)
                                .addComponent(l2))
                        .addComponent(b1)
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(l1)
                                .addComponent(t1)
                                .addComponent(b1))
                        .addComponent(l2)

        );

        getContentPane().add(p1);
    }

    void addGoButtonActionListener(ActionListener e) {
        b1.addActionListener(e);
    }

    String getTextField() {
        return t1.getText();
    }

    void setLabelMessage(String s) {
        l2.setText(s);
    }
}

package dtu.softwarehouse;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Employee {
    private static List<String> listOfActivities = null;
    private static String employeeID;
    private String loggedIn = "";

    //Generates an ID for the employee with name s
    private static String createID(String s, int i) {
        String ID = "";
        if (i != 0) {
            s = s.substring(0, s.length() - 2) + " " + i;
        }
        String[] parts = s.split(" ");
        if (parts.length == 2) {
            ID = "" + parts[0].charAt(0) + parts[0].charAt(1) + parts[1].charAt(0);
        } else if (parts.length > 2) {
            ID = "" + parts[0].charAt(0) + parts[parts.length - 2].charAt(0) + parts[parts.length - 1].charAt(0);
        } else if (parts.length == 1) {
            ID = "" + parts[0].charAt(0) + parts[0].charAt(1) + parts[0].charAt(2);
        }
        ID = ID.toLowerCase();

        if (!employeeExists(ID)) {
            return (ID);
        } else {
            i++;
            return createID(s, i);
        }
    }

    //Checks if ID exists
    public static boolean employeeExists(String id) {
        Path filePath = Paths.get("employees/" + id + "_assigned.txt");
        return Files.exists(filePath);
    }

    public static boolean employeeAndIDExists(String name) {
        List<String> listOfEmployees = ReadWriteFiles.readFromFile("employees", "employees");
        for (String listOfEmployee : listOfEmployees) {
            String[] split = (listOfEmployee.split(";"));
            if (split[0].equals("" + name) && split[1].equals("" + employeeID)) {
                return true;
            }
        }
        return false;
    }

    //Returns true if someone is logged in, else false
    public boolean isLoggedIn() {
        boolean isLoggedIn = true;
        if ((loggedIn.equals(""))) {
            isLoggedIn = false;
        }
        return isLoggedIn;
    }

    //Returns who is logged in
    public String getLoggedIn() {
        return loggedIn;
    }

    //Sets logged in to the user ID
    public void setLoggedIn(String s) throws Exception {
        if (!employeeExists(s)) {
            throw new Exception("");
        }
        this.loggedIn = s;
    }

    //Generates a new employee and the following files
    public String makeNewEmployee(String name) {
        employeeID = createID(name, 0);
        ReadWriteFiles.createDirectoryAtPath("", "employees");
        ReadWriteFiles.createTxtFileAtPath("employees", employeeID + "_worked");
        ReadWriteFiles.createTxtFileAtPath("employees", employeeID + "_assigned");
        ReadWriteFiles.writeLineToFile("employees", "employees", name + ";" + employeeID);
        return employeeID;
    }

    //Method to register time spent
    public void registerWorkingHours(String activity, String time, String loggedIn) throws OperationNotAllowedException {
        if (!employeeExists(loggedIn) || !activityExist(activity)) {
            throw new OperationNotAllowedException("");
        }
        //Writes how much has been worked to personal file
        ReadWriteFiles.writeLineToFile("employees", loggedIn + "_worked", "Worked: " + activity + ", " + time);


        //Add hours to activity timespent file
        listOfActivities = ReadWriteFiles.readFromFile("employees", "Activities");
        for (int i = 0; i < listOfActivities.size(); i++) {
            String[] split = (listOfActivities.get(i)).split(";");
            if (split[0].equals("Act: " + activity)) {
                double newCalculatedHours = Double.parseDouble(split[1]) + Double.parseDouble(time);
                String NewTextLine = "Act: " + activity + ";" + newCalculatedHours;
                ReadWriteFiles.editTextAtLine("employees", "Activities", NewTextLine, i);
            }
        }
    }


    public void assignActivity(String loggedIn, String activity) throws OperationNotAllowedException {
        if (!activityExist(activity) || activityAlreadyAssigned(loggedIn, activity)) {
            throw new OperationNotAllowedException("Employee or activity doesnt exist or activity is already assigned");
        }
        ReadWriteFiles.writeLineToFile("employees", loggedIn + "_assigned", "Assigned: " + activity);
    }


    double seeTimesSpentOnActivity(String activity) throws OperationNotAllowedException {
        double timespent = Double.NaN;
        listOfActivities = ReadWriteFiles.readFromFile("employees", "Activities");
        for (String s : listOfActivities) {
            String[] split = s.split(";");
            if (split[0].equals("Act: " + activity)) {
                timespent = Double.parseDouble(split[1]);
            }
        }

        if (Double.isNaN(timespent)) {
            throw new OperationNotAllowedException("");
        } else return timespent;
    }


    //Checks if activity exist
    public boolean activityExist(String activity) {
        boolean bool = false;
        listOfActivities = ReadWriteFiles.readFromFile("employees", "Activities");
        for (String s : listOfActivities) {
            String[] split = s.split(";");
            if (split[0].equals("Act: " + activity)) {
                bool = true;
            }
        }
        return bool;
    }


    //Checks if activity is already assigned to person
    public boolean activityAlreadyAssigned(String loggedIn, String activity) {
        boolean bool = false;
        List<String> listOfAssigned = ReadWriteFiles.readFromFile("employees", loggedIn + "_assigned");
        for (String s : listOfAssigned) {
            String[] split = s.split(";");
            if (split[0].equals("Assigned: " + activity)) {
                bool = true;
            }
        }
        return bool;
    }


    public void makeNewActivity(String activity) throws OperationNotAllowedException {
        if (activityExist(activity)) {
            throw new OperationNotAllowedException("Activity already exist");
        }
        ReadWriteFiles.writeLineToFile("employees", "Activities", "Act: " + activity + ";0");
    }


    //See employee working hours
    double seeEmployeeWorkingHours(String employee) throws OperationNotAllowedException {
        if (!employeeExists(employee)) {
            throw new OperationNotAllowedException("");
        }
        double total = 0;
        List<String> listOfOwnWorkingHours = ReadWriteFiles.readFromFile("employees", employee + "_worked");
        for (String s : listOfOwnWorkingHours) {
            String[] split = s.split(" ");
            total += Double.parseDouble(split[2]);
        }
        return total;


    }

    public boolean checkLastRegistered(String userID, String activity, String time) {
        List<String> listOfOwnWorkingHours = ReadWriteFiles.readFromFile("employees", userID + "_worked");
        String LastRow = listOfOwnWorkingHours.get(listOfOwnWorkingHours.size() - 1);
        return LastRow.equals("Worked: " + activity + ", " + time);

    }


}
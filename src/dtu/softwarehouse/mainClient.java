package dtu.softwarehouse;

import javax.swing.*;

public class mainClient extends JFrame {

    private static loginScreen loginWindow;
    private static Employee employee;
    private static String loggedIn = "";


    public static void main(String[] args) {
        setupLoginScreen();
    }

    private static void setupLoginScreen() {
        loginWindow = new loginScreen();
        loginWindow.addGoButtonActionListener(e -> {
            loginWindow.setLabelMessage("Connecting...");
            loggedIn = loginWindow.getTextField();
            employee = new Employee();
            if (Employee.employeeExists(loggedIn)) {
                setupMainScreen();
            } else {
                loginWindow.setLabelMessage("ID not recognized.");
            }
        });
        makePretty(loginWindow, "Please login", 360, 120);
    }

    private static void setupMainScreen() {
        loginWindow.setLabelMessage("Login successful.");
        mainProgramScreen GUI = new mainProgramScreen();
        loginWindow.dispose();
        makePretty(GUI, "Workspace manager", 840, 480);
        GUI.setupAssignedScreen(loggedIn);
        GUI.addViewActivitiesButton(e -> {
            //reset the area
            GUI.clearAllTextField();
            GUI.setupAssignedScreen(loggedIn);
        });


        //Add working hours
        GUI.addWorkedHoursButton(e -> {

            String activity = GUI.getWorkedHoursField1();
            String time = GUI.getWorkedHoursField2();

            try {
                employee.registerWorkingHours(activity, time, loggedIn);
                JOptionPane.showMessageDialog(null, "Registered " + time + " hours of work time from user " + loggedIn + " on " + activity);
                GUI.clearAllTextField();
            } catch (OperationNotAllowedException e1) {
                JOptionPane.showMessageDialog(null, "Activity does not exist");
                GUI.clearAllTextField();
            }

        });

        //Add activity
        GUI.addAddActivityButton(e -> {
            String activity = GUI.getAddActivityField1();
            String ID = GUI.getAddActivityField2();

            try {
                employee.assignActivity(ID, activity);
                JOptionPane.showMessageDialog(null, activity + " was assigned to worker " + ID);
                GUI.clearAllTextField();
            } catch (OperationNotAllowedException e2) {
                JOptionPane.showMessageDialog(null, "Either worker or activity doesn't exist or worker is already assigned the activity.");
                GUI.clearAllTextField();
            }


        });
        GUI.addNewEmployeeButton(e -> {
            try {
                String newEmployeeID = employee.makeNewEmployee(GUI.getNewEmployeeNewField());
                JOptionPane.showMessageDialog(null, "New employee created with ID: " + newEmployeeID);
                GUI.clearAllTextField();
            } catch (Exception ignored) {
            }

        });


        GUI.addActivityWorkedHoursButton(e -> {
            String activityName = GUI.getActivityWorkedHours();
            try {
                double WorkedHours = employee.seeTimesSpentOnActivity(activityName);
                JOptionPane.showMessageDialog(null, "Total time worked on " + activityName + " is: " + WorkedHours);
                GUI.clearAllTextField();

            } catch (OperationNotAllowedException e1) {
                JOptionPane.showMessageDialog(null, "No activity with the name: " + activityName);
                GUI.clearAllTextField();

            }


        });


        //What happens when you press the button in pane add activity
        GUI.addNewActivityButton(e -> {
            String activityName = GUI.getNewActivity();
            try {
                employee.makeNewActivity(activityName);
                JOptionPane.showMessageDialog(null, activityName + " is created");
                GUI.clearAllTextField();
            } catch (OperationNotAllowedException e1) {
                JOptionPane.showMessageDialog(null, "Error: " + activityName + " is already created");
                GUI.clearAllTextField();
            }


        });


        //See employee working hours 
        GUI.addEmployeeWorkedHoursButton(e -> {
            String employeeName = GUI.getEmployeeWorkingHours();
            try {
                double workingHours = employee.seeEmployeeWorkingHours(employeeName);
                JOptionPane.showMessageDialog(null, "Employee " + employeeName + " has worked " + workingHours + " hours");
                GUI.clearAllTextField();
            } catch (OperationNotAllowedException e1) {
                JOptionPane.showMessageDialog(null, "Employee " + employeeName + " does not exist");
                GUI.clearAllTextField();
            }

        });

    }

    private static void makePretty(JFrame frame, String title, int width, int height) {
        frame.setTitle(title);
        frame.setSize(width, height);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public static String getLoggedIn() {
        return loggedIn;
    }


    public static void setLoggedIn(String s) {
        loggedIn = s;
    }
}

$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("use_cases/provided/add_employee.feature");
formatter.feature({
  "name": "Add employees to the project",
  "description": "        Description: Employees are added to the project\n        Actors: Employee",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Add an employee successfully",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the employee \"gjj\" logs in",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.theUserLogsIn(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I add the employee \"Random employee\"",
  "keyword": "When "
});
formatter.match({
  "location": "add_employee.iAddTheEmployee(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee \"Random employee\" exist",
  "keyword": "Then "
});
formatter.match({
  "location": "add_employee.theEmployeeExist(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Add an employee when the employee is not logged in",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "that the employee is not logged in",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.thatTheEmployeeIsNotLoggedIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I add the employee \"Random employee\"",
  "keyword": "When "
});
formatter.match({
  "location": "add_employee.iAddTheEmployee(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I get an error message \"Employee must be logged in\"",
  "keyword": "Then "
});
formatter.match({
  "location": "add_employee.iGetAnErrorMessage(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/provided/assign_activity.feature");
formatter.feature({
  "name": "Add employees to the activity",
  "description": "        Description: Employees are assigned an activity by another Employee\n        Actors: Employee",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Add an employee successfully",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "there is an activity \"HEJSA\"",
  "keyword": "Given "
});
formatter.match({
  "location": "assign_activity.thereIsAnActivity(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee with ID \"gjj\" exist",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.theEmployeeWithIDExist(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee \"gjj\" is not already assigned to the activity",
  "keyword": "And "
});
formatter.match({
  "location": "assign_activity.theEmployeeIsNotAlreadyAssignedToTheActivity(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I assign the activity",
  "keyword": "When "
});
formatter.match({
  "location": "assign_activity.iAssignTheActivity()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the activity is assigned to the employee",
  "keyword": "Then "
});
formatter.match({
  "location": "assign_activity.theActivityIsAssignedToTheEmployee()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Add an employee when the employee is not logged in",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "there is no activity \"Doesnt exist\"",
  "keyword": "Given "
});
formatter.match({
  "location": "assign_activity.thereIsNoActivity(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee with ID \"gjj\" exist",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.theEmployeeWithIDExist(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee \"gjj\" is not already assigned to the activity",
  "keyword": "And "
});
formatter.match({
  "location": "assign_activity.theEmployeeIsNotAlreadyAssignedToTheActivity(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I assign the activity",
  "keyword": "When "
});
formatter.match({
  "location": "assign_activity.iAssignTheActivity()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i get the error message \"Employee or activity doesnt exist or activity is already assigned\"",
  "keyword": "Then "
});
formatter.match({
  "location": "assign_activity.iGetTheErrorMessage(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/provided/create_activity.feature");
formatter.feature({
  "name": "Create activity",
  "description": "         Description: An Activity is created\n         Actors: Employee",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Create activity successfully",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the employee \"gjj\" logs in",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.theUserLogsIn(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I create the activity",
  "keyword": "When "
});
formatter.match({
  "location": "create_activity.iCreateTheActivity()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "there is created an activity with the name \"Activity_name\"",
  "keyword": "Then "
});
formatter.match({
  "location": "create_activity.thereIsCreatedAnActivityWithTheName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/provided/employee_login.feature");
formatter.feature({
  "name": "Employee login",
  "description": "\tDescription: The employee logs into the system and also logs out\n\tActor: Employee\t",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Employee can login",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "that the employee is not logged in",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.thatTheEmployeeIsNotLoggedIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee with ID \"gjj\" exist",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.theEmployeeWithIDExist(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee \"gjj\" logs in",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.theUserLogsIn(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee is logged in",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.theEmployeeIsLoggedIn()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Employee has the wrong username",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "that the employee is not logged in",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.thatTheEmployeeIsNotLoggedIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee \"Something\" doesnt exist",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.theUserDoesNotExist(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee \"Something\" logs in",
  "keyword": "When "
});
formatter.match({
  "location": "LoginSteps.theUserLogsIn(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the employee is not logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.theEmployeeIsNotLoggedIn()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/provided/register_workinghours.feature");
formatter.feature({
  "name": "Register working hours",
  "description": "        Description: Employee registers working hours on activity\n        Actors: Employee",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Register working hours",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the employee \"gjj\" logs in",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.theUserLogsIn(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I have an activity \"Design\"",
  "keyword": "And "
});
formatter.match({
  "location": "register_workinghours.iHaveAnActivity(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I register \"3\" working hours",
  "keyword": "When "
});
formatter.match({
  "location": "register_workinghours.iRegisterWorkingHours(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the working hours are registered with ID \"gjj\" and activity \"Design\"",
  "keyword": "Then "
});
formatter.match({
  "location": "register_workinghours.theWorkingHoursAreRegisteredWithIDAndActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
});